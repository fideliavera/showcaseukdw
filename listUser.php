<?php
	session_start();
	include('connect.php');
?>

 <!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<div id="utama">
	
		
		<?php
		include "header.php"
		?>
			
		<div class="container">
			<nav id="menu1">
				<ul>
					<li class="menu"><a href="editPost.php">Edit Post</a></li>
					<li class="menu"><a href="requestPost.php">Request Post</a></li>
					<li class="menu aktif"><a href="listUser.php">List User</a></li>
				</ul>
			</nav>

			<div id="containIsi">
				<table id="listUser">
					<tr id="judulTabel">
						<td>No.</td>
						<td>Photo</td>
						<td>Fullname</td>
						<td>Username</td>
						<td>NIM</td>
						<td>Email</td>
						<td>Password</td>
					</tr>
					<?php
						$i=1;

					    $sql = "SELECT * FROM user WHERE user_admin=0 ORDER BY user_username ASC";

					    $q = mysqli_query($conn,$sql);
					    while ( $row = mysqli_fetch_array($q)){ 
					?>
						<tr>
							<td><?php echo $i++ ?></td>
							<td><img class="photoTable" src="img/user/<?php echo $row['user_image']?>"></td>
							<td><?php echo $row['user_fullname']?></td>
							<td><?php echo $row['user_username']?></td>
							<td><?php echo $row['user_nim']?></td>
							<td><?php echo $row['user_email']?></td>
							<td><?php echo $row['user_password']?></td>
						</tr>		

					<?php
						}	
					?>
					
				</table>
					</div>
			
			<?php
			include "search.php"
			?>

		</div> 

        <?php
		include "footer.php"
		?>

    </div>
	 
</body>

</html>


