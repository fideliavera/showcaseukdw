<?php
	session_start();
?>
<?php
	include('connect.php');
?>
<!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<div id="utama">
	
		
		<?php
		include "header.php"
		?>
			
		<div class="container">
			<nav id="menu">
				<ul>
				<?php
				if(isset($_SESSION['nim'])){
					if($_SESSION['admin']){?>
						<li class="menu aktif"><a href="admin.php">Message</a></li>
						<li class="menu"><a href="editPost.php">Edit Post</a></li>
						<li class="menu"><a href="requestPost.php">Request Post</a></li>
						<li class="menu"><a href="listUser.php">List User</a></li>
					<?php
					}
				}else{
				?>
					<li class="menu aktif"><a href="index.php">Home</a></li>
					<li class="menu"><a href="new.php">New</a></li>
					<li class="menu"><a href="category.php">Category</a></li>
					<li class="menu"><a href="about.php">About</a></li>
					<?php } ?>
				</ul>
			</nav>

			<div id="containIsi">
				<?php
					
				    $search = $_GET['search'];

				    $sql = "SELECT * FROM user  WHERE user_username LIKE '%$search%' OR user_fullname LIKE '%$search%'";

				    $q = mysqli_query($conn,$sql);
				    if (mysqli_num_rows($q)<1){
				    	echo "<span class='noresult'>NO RESULT</span>";
				    }
				    else{

						    while ( $row = mysqli_fetch_array($q)){ 
						?>
								<a href="myProfil.php?id=<?php echo $row['user_id'];?>">    	
									<div class="searchview">
										<div class="gmbr">
											<img class="searchimg" src="img/user/<?php echo $row['user_image'];?>" alt="<?php echo $row['user_username']?>"/>								
										</div>
										<div>
											<h5 class="itemsearch"> <?php echo $row['user_username'] ?></h5>
											<h6 class="itemsearch"> <?php echo $row['user_fullname']?></h6>
											<h6 class="itemsearch"> <?php echo $row['user_nim']?></h6>	
											<h6 class="itemsearch"> <?php echo $row['user_email']?></h6>
										</div>
										
									</div>
								</a>

							<?php }

						    $sql = "SELECT * FROM karya INNER JOIN user ON karya_creator = user_id WHERE karya_accept = 1 AND (karya_judul LIKE '%$search%' OR user_fullname LIKE '%$search%' OR user_username LIKE '%$search%')";

						    $q = mysqli_query($conn,$sql);
						    while ( $row = mysqli_fetch_array($q)){ 
						?>
								<a href="detail.php?id=<?php echo $row['karya_id'];?>">    	
									<div class="searchview">
										<div class="gmbr">
											<img class="searchimg" src="img/karya/<?php echo $row['karya_image'];?>" alt="<?php echo $row['karya_details']?>"/>								
										</div>
										<div>
											<?php
											if(isset($_SESSION['nim'])){
												if($_SESSION['admin']){?>
													<p class="karyadel"><a href="delete1.php?id=<?php echo $row['karya_id']?>">x</a></p>
												<?php
												}
											}
											?>
											<h5 class="itemsearch"> <?php echo $row['karya_judul'] ?></h5>	
											<h6 class="itemsearch"> <?php echo $row['user_username']?></h6>
											<h6 class="itemsearch"> <?php echo $row['user_fullname']?></h6>
											<h6 class="itemsearch"> <?php echo $row['karya_tanggal']?></h6>	
										</div>
										
									</div>
								</a>			

						<?php
							}	
						}
						?>	
				    
							
			</div>
			
			<?php
	            include "search.php";
	        ?>

		</div> 

        <?php
		include "footer.php"
		?>

    </div>
	 
</body>

</html>







	