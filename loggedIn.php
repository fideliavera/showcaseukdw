<?php
	session_start();
?>

 <!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />

</head>

<body>
	<div id="utama">
	
		
		<?php
			include "header.php"
		?>
			
		<div class="container">
			<nav id="menu">
				<ul>
					<li class="menu aktif"><a href="index.php">Home</a></li>
					<li class="menu"><a href="new.php">New</a></li>
					<li class="menu"><a href="category.php">Category</a></li>
					<li class="menu"><a href="about.php">About</a></li>
				</ul>
			</nav>

			<div id="containIsi">
				<a href="">
					<div class="gallery">
						<img class="foto" src="css/img/colTree.jpg" alt="Colorful Tree"/>
						<h5>Colorful Tree</h5>
					</div>
				</a>

				<a href="">
					<div class="gallery">
						<img class="foto" src="css/img/camMan.jpg" alt="Cammera Man"/>
						<h5>The Camera Man</h5>
					</div>
				</a>

				<a href="detail.php">
					<div class="gallery">
						<img class="foto" src="css/img/angrybird.png" alt="Angry Bird"/>
						<h5>Angry Bird</h5>
					</div>
				</a>	

				<a href="">
					<div class="gallery">
						<img class="foto" src="css/img/wechat.jpg" alt="We Chat"/>
						<h5>We Chat</h5>
					</div>
				</a>	

				<a href="">
					<div class="gallery">
						<img class="foto" src="css/img/mario.png" alt="Super Mario"/>
						<h5>Super Mario</h5>
					</div>
				</a>

				<a href="">
					<div class="gallery">
						<img class="foto" src="css/img/kakao.png" alt="Kakao Talk"/>
						<h5>Kakao Talk</h5>
					</div>
				</a>

				<a href="">
					<div class="gallery">
						<img class="foto" src="css/img/waze.png" alt="Waze"/>
						<h5>Waze</h5>
					</div>
				</a>

				<a href="">
					<div class="gallery">
						<img class="foto" src="css/img/cytus.png" alt="Cytus"/>
						<h5>Cytus</h5>
					</div>
				</a>
			</div>

			<div id="containSearch">
				<input type="text"/>
				<a href="">
					<div id="kotakSearch">
						<img id="tombolSearch" src="css/img/search.png" alt="Search"/>
					</div>
				</a>
			</div>

		</div> 

        <div id="footer">
            <p>&copy; NamaKelompok </p>
            <img class="icon" src="css/img/email.jpg" alt="Email"/>&nbsp;NamaKelompok@ukdw.ac.id&nbsp;&nbsp;&nbsp;<img class="icon" src="css/img/facebook.png" alt="Facebook"/>&nbsp;NamaKelompok&nbsp;&nbsp;&nbsp;<img class="icon" src="css/img/twitter.png" alt="Twitter"/>&nbsp;@NamaKelompok
        </div>

    </div>
	 
</body>

</html>


