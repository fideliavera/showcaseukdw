<?php
	session_start();
	include "connect.php";

	if (isset($_POST['submit'])){
		$b = "UPDATE karya SET karya_judul = '$_POST[title]', karya_details = '$_POST[detail]', karya_link= '$_POST[link]' WHERE karya_id='$_GET[id]'";
		mysqli_query($conn,$b);
		
		header("location: detail.php?id=" . $_GET['id']);
	}

?>
 <!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<div id="utama">
	
		
		<?php
		include "header.php"
		?>
			
		<div class="container">
			<nav id="menu">
				<ul>
					<li class="menu aktif"><a href="index.php">Home</a></li>
					<li class="menu"><a href="new.php">New</a></li>
					<li class="menu"><a href="category.php">Category</a></li>
					<li class="menu"><a href="about.php">About</a></li>
				</ul>
			</nav>

			<div id="containIsi">
				<?php
				$a = "SELECT * FROM karya WHERE karya_id = '$_GET[id]'";
				$result = mysqli_query($conn,$a);
				$row = mysqli_fetch_array($result);

				?>
				<form enctype="multipart/form-data" action="editkarya.php?id=<?php echo $_GET['id'] ?>" method="POST">
	                <p class="biru">Judul Karya</p> <input type="text" name="title" id="judulKarya" value="<?php echo $row['karya_judul']?>">
	                <p class="biru">Detail Singkat</p> <textarea type="text" name="detail" id="detailSingkat"><?php echo $row['karya_details']?></textarea>
	                <p class="biru">Link</p> <textarea type="text" name="link" id="link"><?php echo $row['karya_link']?></textarea>
	                
	                <button type="submit" id="buttonSubmit" name="submit">Submit</button>
	            </form>
			</div>
			
			<?php
			include "search.php"
			?>

		</div> 

        <?php
		include "footer.php"
		?>

    </div>
	 
</body>

</html>
