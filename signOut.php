 <!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

</head>

<body>
	<div id="utama">
			
		<?php
		include "header.php"
		?>
			
		<div class="container">
			<nav id="menu">
				<ul>
					<li class="menu"><a href="index.php">Home</a></li>
					<li class="menu"><a href="new.php">New</a></li>
					<li class="menu"><a href="category.php">Category</a></li>
					<li class="menu"><a href="about.php">About</a></li>
				</ul>
			</nav>

			<h2 class="header">Thank You!</h2>

			<?php
			include "search.php"
			?>

		</div> 

        <?php
		include "footer.php"
		?>
    </div>
	 
</body>

</html>


