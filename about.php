<?php
	session_start();
?>

<!DOCTYPE html>

<html>

	<head>
		<title>About Us</title>
		<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>

	<body>
		<div id="utama">
		
			<?php
			include "header.php"
			?>
				
			<div class="container">
				<nav id="menu">
					<ul>
						<li class="menu"><a href="index.php">Home</a></li>
						<li class="menu"><a href="new.php">New</a></li>
						<li class="menu"><a href="category.php">Category</a></li>
						<li class="menu aktif"><a href="about.php">About</a></li>
					</ul>
				</nav>

				<h2>Crew of ShowCase UKDW</h2>

				<hr>
				<img class="profPict" src="css/img/vera.jpg" alt="fideliavera"/>
				<table class="bio">
					<tr><td><h3 class="name">Fidelia Vera Sentosa</h3></td></tr>
					<tr><td>&nbsp;Fullname&nbsp;&nbsp;: Fidelia Vera Sentosa</td></tr>
					<tr><td>&nbsp;Username: fideliavera</td></tr>
					<tr><td>&nbsp;Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: fideliavera@ti.ukdw.ac.id</td></tr>
					<tr><td>&nbsp;Facebook&nbsp;&nbsp;: <a href="https://www.facebook.com/fidelia.verasentosa/">Fidelia Vera Sentosa</a></td></tr>
					<tr><td>&nbsp;Account&nbsp;&nbsp;&nbsp;&nbsp;: <a href="myProfil.php?id=1">fideliavera</a></td></tr>
					<tr><td>&nbsp;Bio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Mahasiswi Teknik Informatika angkatan 2014 Universitas Kristen Duta Wacana</td></tr>
				</table>
				<hr>

				<img class="profPict" src="css/img/karlina.jpg" alt="karlina"/>
				<table class="bio">
					<tr><td><h3 class="name">Karlina</h3></td></tr>
					<tr><td>&nbsp;Fullname&nbsp;&nbsp;: Karlina</td></tr>
					<tr><td>&nbsp;Username: crlyna</td></tr>
					<tr><td>&nbsp;Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Karlina@ti.ukdw.ac.id</td></tr>
					<tr><td>&nbsp;Facebook&nbsp;&nbsp;: <a href="https://www.facebook.com/crllna/">Karlina</a></td></tr>
					<tr><td>&nbsp;Account&nbsp;&nbsp;&nbsp;&nbsp;: <a href="myProfil.php?id=2">karlina</a></td></tr>
					<tr><td>&nbsp;Bio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Mahasiswi Teknik Informatika angkatan 2014 Universitas Kristen Duta Wacana</td></tr>
				</table>

				<hr>
				<img class="profPict" src="css/img/agatha.jpg" alt="agathamalino"/>
				<table class="bio">
					<tr><td><h3 class="name">Agatha Kristianto Malino</h3></td></tr>
					<tr><td>&nbsp;Fullname&nbsp;&nbsp;: Agatha Kristianto Malino</td></tr>
					<tr><td>&nbsp;Username: agathamalino</td></tr>
					<tr><td>&nbsp;Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: agathamalino@ti.ukdw.ac.id<br></tr>
					<tr><td>&nbsp;Facebook&nbsp;&nbsp;: <a href="https://www.facebook.com/aghamalino/">Agatha K Malino</a></td></tr>
					<tr><td>&nbsp;Account&nbsp;&nbsp;&nbsp;&nbsp;: <a href="myProfil.php?id=4">agthakm</a></td></tr>
					<tr><td>&nbsp;Bio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Mahasiswa Teknik Informatika angkatan 2011 Universitas Kristen Duta Wacana</td></tr>
				</table>

				<hr>
				<img class="profPict" src="css/img/ega.jpg" alt="egaadithya"/>
				<table class="bio">
					<tr><td><h3 class="name">I Gusti Ngurah Ega</h3></td></tr>
					<tr><td>&nbsp;Fullname&nbsp;&nbsp;: I Gusti Ngurah Ega</td></tr>
					<tr><td>&nbsp;Username: egadithya</td></tr>
					<tr><td>&nbsp;Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: egadithya@ti.ukdw.ac.id</td></tr>
					<tr><td>&nbsp;Facebook&nbsp;&nbsp;: <a href="https://www.facebook.com/Svck.Live/">I Gusti Ngurah Ega</a></td></tr>
					<tr><td>&nbsp;Account&nbsp;&nbsp;&nbsp;&nbsp;: <a href="myProfil.php?id=5">egaditya</a></td></tr>
					<tr><td>&nbsp;Bio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Mahasiswa Teknik Informatika angkatan 2011 Universitas Kristen Duta Wacana</td></tr>
				</table>
		
				<hr>
				<?php
				include "search.php"
				?>

			</div>

			<?php
			include "footer.php"
			?>

		</div>
			
			
		
	</body>

</html>


