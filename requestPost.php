<?php
	session_start();
?>

<?php
	include('connect.php')
?>

 <!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<div id="utama">
	
		
		<?php
		include "header.php"
		?>
			
		<div class="container">
			<nav id="menu1">
				<ul>
					<li class="menu"><a href="editPost.php">Edit Post</a></li>
					<li class="menu aktif"><a href="requestPost.php">Request Post</a></li>
					<li class="menu"><a href="listUser.php">List User</a></li>
				</ul>
			</nav>

			<div id="containIsi">

				<table id="listUser">
					<tr id="judulTabel">
						<td>No.</td>
						<td>Fullname</td>
						<td>NIM</td>
						<td>Creation</td>
						<td>Reaction</td>
					</tr>
					<?php
						$sql = "SELECT karya_id, karya_creator, karya_judul, karya_details, karya_link, karya_tanggal, karya_accept FROM karya WHERE karya_accept = '0'";
						$result = mysqli_query($conn,$sql);

						$i=1;
						


						        // echo "id: " . $row["user_id"]. " - Fullname: " . $row["user_fullname"]. " - Username " . $row["user_username"]. " - Email: " . $row["user_email"]. " - Facebook: " . $row["user_facebookname"]."<br>";
		                while($row = mysqli_fetch_array($result)){

							$sqlKarya = "SELECT user_fullname, user_nim FROM user WHERE user_id = '$row[karya_creator]'";
			                $result1 = $conn->query($sqlKarya);
			                $row1 = mysqli_fetch_array($result1);
		                	?>
							<tr>
								<td><?php echo $i ?></td>
								<td><?php echo $row1['user_fullname'];?></td>
								<td><?php echo $row1['user_nim'];?></td>
								<td><a href="detail.php>id=<?php echo $row['karya_id']?>"><?php echo $row['karya_judul']?></a></td>
								<td><a href="acceptProcess.php?id=<?php echo $row['karya_id']?>">Accept</a>&nbsp;&nbsp;&nbsp;<a href="decline.php?id=<?php echo $row['karya_id']?>"> Decline</a></td>
							</tr>
							<?php
							$i++;
						}
					?>
				</table>
			</div>
			
			<?php
			include "search.php"
			?>

		</div> 

        <?php
		include "footer.php"
		?>

    </div>
	 
</body>

</html>