<?php
    session_start();
?>

<!DOCTYPE html>

<html>

<head>
	<title>Profil</title>
    <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>    
<body>
    <div id="utama">                       
        <?php
        include "header.php"
        ?>
            
        <div class="container">
            <nav id="menu">
                <ul>
                <li class="menu"><a href="index.php">Home</a></li>
                <li class="menu"><a href="new.php">New</a></li>
                <li class="menu"><a href="category.php">Category</a></li>
                <li class="menu"><a href="about.php">About</a></li>
                </ul>
            </nav>

            <form action="mail.php" method="POST">
                <p>Fullname</p> <input type="text" name="name">
                <p>Username</p> <input type="text" name="user">
                <p>Email (@ti.uksw.ac.id)</p> <input type="text" name="email">
                <p>Password</p> <input type="text" name="pass">
                <p>Re-enter Password</p> <input type="text" name="rePass">
            </form>

            <a href="Submit.php">
                <div class="button" id="submit">Submit</div>
            </a>
        </div>

		<?php
        include "search.php"
        ?>              

        <?php
        include "footer.php"
        ?>
    </div>

</body>

</html>