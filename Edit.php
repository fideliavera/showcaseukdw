<?php
	session_start();
	if(isset($_SESSION['id'])){
		if($_SESSION['nim'] != 'admin'){	
?>
<!DOCTYPE html>

<html>

	<head>
		<title>Edit Profil</title>
	    <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>

 	<body>

        <div id="utama">
            
            <?php
            include "header.php"
            ?>
            	<div class="content">
	<!-- <span>Profile</span><hr><br> -->
		<div class="Profile">
			<!-- <img src="image/karlina.jpg" alt="Profile" class="fotoProfile"> -->
		    <div class="detailProfile">
		   		<?php
		    		$tamp = $_SESSION['nim']; 
					include "connect.php";
					$sql =  "SELECT user_fullname, user_nim, user_email, user_facebookname, user_facebooklink, user_image, user_admin FROM user WHERE user_nim = '$tamp'";
					$result = mysqli_query($conn, $sql);
					$row = mysqli_fetch_array($result);
						
					
				?>
        	</div>
    	</div>
                
            <div class="container"> 
                <nav id="menu">
                    <ul>
                    <li class="menu"><a href="index.php">Home</a></li>
                    <li class="menu"><a href="new.php">New</a></li>
                    <li class="menu"><a href="category.php">Category</a></li>
                    <li class="menu"><a href="about.php">About</a></li>
                </ul>
                </nav>

                <form class="nginput" action="editSuccess.php" method="POST">
                    <p class="biru">Fullname</p> <input type="text" name="name" class="kotak" value="<?php echo ($row['user_fullname'])?>">
                    <p class="biru">Username</p> <input type="text" name="user" class="kotak" value="<?php echo ($row['user_nim'])?>">
                    <p class="biru">Email</p> <input type="text" name="email" class="kotak" value="<?php echo ($row['user_email'])?>">
                    <p class="biru">Password</p> <input type="text" name="pass" class="kotak">
                    <p class="biru">Re-enter Password</p> <input type="text" name="rePass" class="kotak">

                    <button type="submit" id="edit">Submit</button>
                    
                </form>            

            </div>

			<?php
            include "search.php";
            ?>

	        <?php
            include "footer.php";
        }
    }
            ?>

        </div>
    
    </body>

</html>
