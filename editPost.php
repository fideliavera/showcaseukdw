<?php
	session_start();
?>
<?php
	include('connect.php');
?>
 <!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<div id="utama">
	
		
		<?php
		include "header.php"
		?>
			
		<div class="container">
			<nav id="menu1">
				<ul>
					<li class="menu aktif"><a href="editPost.php">Edit Post</a></li>
					<li class="menu"><a href="requestPost.php">Request Post</a></li>
					<li class="menu"><a href="listUser.php">List User</a></li>
				</ul>
			</nav>

			<div id="containIsi">
				<?php

	                $sql = "SELECT karya_id, karya_creator, karya_judul, karya_image, karya_category, karya_details, karya_link, karya_tanggal FROM karya WHERE karya_accept = 1 ORDER BY karya_judul ASC LIMIT 0,8";
	                $result = $conn->query($sql);

	                while($row = mysqli_fetch_array($result)){
	                	?>
	            		<a href="detail.php?id=<?php echo $row['karya_id'];?>">    	
							<div class="gallery">
								<p class="del"><a href="delete.php?id=<?php echo $row['karya_id']?>">x</a></p>
			               		<img class="foto" src="img/karya/<?php echo $row['karya_image'];?>" alt="<?php echo $row['karya_details']?>"/>
							<h5> <?php echo $row['karya_judul'] ?></h5>
							</div>
						</a>
						<?php
					}

				?>
				
			</div>
			
			<?php
			include "search.php"
			?>

		</div> 

        <?php
		include "footer.php"
		?>

    </div>
	 
</body>

</html>


