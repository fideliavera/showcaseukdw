<?php
	session_start();
?>

 <!DOCTYPE html>

<html>

<head>
	<title>Show Case UKDW</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
</head>

<body>
	<div id="utama">
	
		
		<?php
		include "header.php"
		?>
			
		<div class="container">
			<nav id="menu">
				<ul>
					<li class="menu aktif"><a href="index.php">Home</a></li>
					<li class="menu"><a href="new.php">New</a></li>
					<li class="menu"><a href="category.php">Category</a></li>
					<li class="menu"><a href="about.php">About</a></li>
				</ul>
			</nav>

			<div id="containIsi">
				<form enctype="multipart/form-data" action="uploadProcess.php" method="POST">
	                <p class="biru">Judul Karya</p> <input type="text" name="title" id="judulKarya">
	                <p class="biru">Kategori</p> 
					  <input type="radio" name="cat" value="art"> Art<br>
					  <input type="radio" name="cat" value="education"> Education<br>
					  <input type="radio" name="cat" value="lifestyle"> Lifestyle<br>
					  <input type="radio" name="cat" value="games"> Games<br>
					  <input type="radio" name="cat" value="mobile"> Mobile Application
	                <p class="biru">Detail Singkat</p> <textarea type="text" name="detail" id="detailSingkat"></textarea>
	                <p class="biru">Link</p> <textarea type="text" name="link" id="link"></textarea> 
	                <p class="biru">Image</p><input type="file" name="img">
	                
	                <button type="submit" id="buttonSubmit">Submit</button>
	            </form>
			</div>
			
			<?php
			include "search.php"
			?>

		</div> 

        <?php
		include "footer.php"
		?>

    </div>
	 
</body>

</html>
