<?php
    session_start();
?>
<?php
    include('connect.php');
?>
<!DOCTYPE html>

<html>

<head>
    <title>Details</title>
    <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</head>
    
<body>

    <div id="utama">
        
         <?php
        include "header.php"
        ?>
            
        <div class="container">
            <nav id="menu">
                <ul>
                    <li class="menu"><a href="index.php">Home</a></li>
                    <li class="menu"><a href="new.php">New</a></li>
                    <li class="menu"><a href="category.php">Category</a></li>
                    <li class="menu"><a href="about.php">About</a></li>
                </ul>
            </nav>

            <?php
                $sql = "SELECT karya_id, karya_creator, karya_judul, karya_image, karya_details, karya_link, karya_tanggal, karya_view FROM karya WHERE karya_id = '$_GET[id]'";
                $result = $conn->query($sql);

                $row = $result->fetch_assoc();

                $a = $row['karya_view'] + 1;
                $sqllll = "UPDATE karya SET karya_view = '$a' WHERE karya_id='$_GET[id]'";
                mysqli_query($conn,$sqllll);

                $sqlCreator = "SELECT user_username FROM user WHERE user_id = '$row[karya_creator]'";
                $result1 = $conn->query($sqlCreator);

                $row1 = $result1->fetch_assoc();



                        // echo "id: " . $row["user_id"]. " - Fullname: " . $row["user_fullname"]. " - Username " . $row["user_username"]. " - Email: " . $row["user_email"]. " - Facebook: " . $row["user_facebookname"]."<br>";
                
            ?>

            <div style="overflow:hidden">
                <img class="detailPict" src="img/karya/<?php echo $row["karya_image"];?>" alt="Angry Bird"/>
                <p class="created">Created by: <a href="myProfil.php?id=<?php echo $row["karya_creator"]?>"><?php echo $row1["user_username"]?></a></p>
                <p class="tglupload">Upload: <?php echo $row['karya_tanggal']?></p>
                <h2 class="detailHeader"><?php echo $row["karya_judul"];?></h2>
                <p class="detail">
                    <?php echo $row["karya_details"];?>
                </p>
    
            </div>
            
            <a href="<?php echo $row['karya_link']?>">
                <div id="download">Download</div>
            </a>
            <?php
                if (isset($_SESSION['nim'])){
                    
                    if($_SESSION['id']==$row['karya_creator']){?>
                        <a href="editkarya.php?id=<?php echo $row['karya_id']?>">
                            <div id="download">Edit</div>
                        </a>
                    <?php
                    }
                }
            ?>
            
            <?php
            ?>
        </div>

        <?php
            include "search.php"
            ?>
    
        <?php
        include "footer.php"
        ?>

    </div>

</body>
    
</html>