<?php
	session_start();
?>

<?php
	include('connect.php')
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Category/Art</title>
        <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
    </head>
	<body>
		<div id="utama">   
            
            <?php
			include "header.php"
			?>
                
            <div class="container">
                <nav id="menu">
                    <ul>
						<li class="menu"><a href="index.php">Home</a></li>
						<li class="menu"><a href="new.php">New</a></li>
						<li class="menu aktif"><a href="category.php">Category</a></li>
						<li class="menu"><a href="about.php">About</a></li>
                    </ul>
                </nav>

		<div class="wrapper">
	        <div class="leftSide">
	        	<header>
			        <nav>
			            <ul>
			                <li class="category aktif"><a href="category.php" class="current">Art</a></li>
			                <li class="category"><a href="education.php">Education</a></li>
			                <li class="category"><a href="lifestyle.php">Lifestyle</a></li>
			                <li class="category"><a href="games.php">Games</a></li>
			                <li class="category"><a href="mobile.php">Mobile Aplication</a></li>
			            </ul>
			        </nav>
		    	</header>	
	        </div>

	        <div class="rightSide">
	        	<aside>
				    <section class="create-popular">
				        <h2>Most Viewed</h2>
				        <?php

			                $sql = "SELECT karya_id, karya_creator, karya_judul, karya_image, karya_category, karya_details, karya_link, karya_tanggal, karya_view FROM karya WHERE karya_accept = 1 ORDER BY karya_view DESC LIMIT 0,4";
			                $result = $conn->query($sql);

			                while($row = mysqli_fetch_array($result)){
			                	?>
			                	<a href="detail.php?id=<?php echo $row['karya_id']?>"><?php echo $row['karya_judul']?></a>
								<?php
							}

						?>
				         <a href="detail.php?id=<?php echo $row['karya_id']?>"><?php echo $row['karya_judul']?></a>
			
				    </section>
		       </aside>
	        </div>

	        <div class="contentSide">
	        	<?php

	                $sql = "SELECT karya_id, karya_creator, karya_judul, karya_image, karya_category, karya_details, karya_link, karya_tanggal FROM karya WHERE karya_category=1";
	                $result = $conn->query($sql);

	                while($row = mysqli_fetch_array($result)){
	                	?>
	            		<a href="detail.php?id=<?php echo $row['karya_id'];?>">    	
							<div class="gallery">
			               		<img class="foto" src="img/karya/<?php echo $row['karya_image'];?>" alt="<?php echo $row['karya_details']?>"/>
							<h5> <?php echo $row['karya_judul'] ?></h5>
							</div>
						</a>
						<?php
					}
				?>
					</div>

	        </div>
	    
		</div>

	</div>
	
        
	<?php
	include "search.php"
	?>
		

	<?php
	include "footer.php"
	?>
        </div>
	</body>


 
</html>