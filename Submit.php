<?php
	session_start();
?>

<!DOCTYPE html>

<html>

	<head>
		<title>Responses</title>
	    <link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<body>

	    <div id="utama">
	        
	            
	        <div id="logo">
	                <a href="http://www.ukdw.ac.id/id/home">
	                    <img id="logoUkdw" src="http://www.ukdw.ac.id/images/logo/logo.png" alt="Universitas Kristen Duta Wacana"/>
	                </a>
	            <h1>UKDW ShowCase</h1>
	            <a href="Profil.php">
	                <div id="buttonprofil">Profil</div>
	            </a>
	        </div>
	            
	        <div class="container">
	            <nav id="menu">
	                <ul>
					<li class="menu"><a href="index.php">Home</a></li>
					<li class="menu"><a href="new.php">New</a></li>
					<li class="menu"><a href="category.php">Category</a></li>
					<li class="menu"><a href="about.php">About</a></li>
	                </ul>
	            </nav>
	        </div>

			<?php
			include "search.php"
			?>

	        <?php
			include "footer.php"
			?>

	    </div>
	</body>
</html>