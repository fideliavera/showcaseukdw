<?php
	session_start();
?>

<?php
	include('connect.php');
?>

 <!DOCTYPE html>

<html>

<head>
	<title>My Profil</title>
	<link href='https://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>

</head>

<body>
	<div id="utama">	
		<div id="logo">
				<a href="http://www.ukdw.ac.id/id/home">
					<img id="logoUkdw" src="http://www.ukdw.ac.id/images/logo/logo.png" alt="Universitas Kristen Duta Wacana"/>
				</a>
			<h1>UKDW ShowCase</h1>
			
			
			<div id="loginSignup">


				<?php 
				if (isset($_SESSION['nim'])){
					
					if($_SESSION['id']==$_GET['id']){?>
						<a href="Edit.php">
							<div class="button login">Edit Profil</div>
						</a>	
					<?php
					}
					?>        

					<a href="signOut.php">
						<div class="button login">Sign Out</div>
					</a>
				<?php 
				}
				else{
					echo "<div class= 'login' onclick='munculin()'>Log In</div>  ";
				}
				?>
				<div id="kotakAtas">
					<form id="formLogin" method="POST">
						<p id="keterangan">NIM & Password must be matched with SSAT</p>
						<label class="label">Nim</label>
						<input class="kotakInput" type="text" placeholder="Inputkan NIM Anda disini" id="nim" name="nim" onblur="validasi()">
						<img id="warning" src="css/img/warning.png" alt="warning"/>
						<label class="label">Password</label>
						<input class="kotakInput" type="password" placeholder="Inputkan Password Anda disini" id="pass" name="pass">
						<div type="submit" id="kotakLogin">
							Log in
						</div>
					</form>
					

				</div>
					
			</div>	
		</div>
			
		<div class="container">
			<nav id="menu">
				<ul>
					<li class="menu"><a href="index.php">Home</a></li>
					<li class="menu"><a href="new.php">New</a></li>
					<li class="menu"><a href="category.php">Category</a></li>
					<li class="menu"><a href="about.php">About</a></li>
				</ul>
			</nav>

			<div id="containIsi">
				<?php
					$sql = "SELECT user_id, user_fullname, user_username, user_nim, user_password, user_email, user_facebookname, user_facebooklink, user_image, user_admin FROM user WHERE user_id = '$_GET[id]'";
					$result = mysqli_query($conn,$sql);
					
					$row = mysqli_fetch_array($result);

					$sqlKarya = "SELECT karya_id, karya_image, karya_judul FROM karya WHERE karya_creator = '$row[user_id]' && karya_accept = 1";
	                $result1 = $conn->query($sqlKarya);

					        // echo "id: " . $row["user_id"]. " - Fullname: " . $row["user_fullname"]. " - Username " . $row["user_username"]. " - Email: " . $row["user_email"]. " - Facebook: " . $row["user_facebookname"]."<br>";
				?>
				<img class="fotoprofil" src="img/user/<?php echo $row['user_image'];?>" alt="<?php echo $row['user_username']?>"/>
				<ul id="ul">
					<li class="rincian">Fullname&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $row['user_fullname']; ?></li>
					<li class="rincian">Username&nbsp;&nbsp;: <?php echo $row['user_username']; ?></li>
					<li class="rincian">NIM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $row['user_nim']; ?></li>
					<li class="rincian">Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $row['user_email']; ?></li>
					<li class="rincian">Facebook&nbsp;&nbsp;&nbsp;&nbsp;:<a href= <?php echo $row['user_facebooklink']; ?>>&nbsp;<?php echo $row['user_facebookname']; ?></a>
					<li class="rincian">Karya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </li>
					
				</ul>
				<div id="karya">
					<?php
		                while($row1 = mysqli_fetch_array($result1)){
		            ?>
					<a href="detail.php?id=<?php echo $row1['karya_id']?>"><div class="kotakan">
						<img class="gambar" src="img/karya/<?php echo $row1['karya_image']?>" alt="Angry Bird"/>
						<p class="title"><?php echo $row1['karya_judul'] ?></p>	
					</div>
					</a>
					<?php
						}
					?>
				</div>
				
				<?php
				if (isset($_SESSION['nim'])){
					
					if($_SESSION['id']==$_GET['id']){?>
						<a href="upload.php">
			                <div id="upload">Upload</div>
			            </a>
					<?php
					}
				}
				?>



			</div>

			<?php
			include "search.php"
			?>

		</div> 

        <?php
		include "footer.php"
		?>

    </div>
	 
</body>

</html>


